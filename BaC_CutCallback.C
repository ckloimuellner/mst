#include "BaC_CutCallback.h"

BaC_CutCallback::BaC_CutCallback(IloCplex& _cplex, IloEnv& _env, IloModel& _model, double _eps, int _V0_num, IloNumVarArray& _f, IloBoolVarArray& _y, bool _lp_bound) :
cplex(_cplex), LazyConsI( _env ), env( _env ), model(_model), eps( _eps ), V0_num(_V0_num), f(_f), y(_y), lp_bound(_lp_bound) {
	//std::cout << "cons start" << std::endl;
	n_itr = 0;
	n_cuts = 0;
	n_red = 0;
}

BaC_CutCallback::~BaC_CutCallback() {
}

int BaC_CutCallback::separate() {
	return findCuts();
}

/*
* separation of cut inequalities
*/
int  BaC_CutCallback::findCuts()
{
	n_itr++;
	int cuts_added = 0;

	//std::cout << "sepa" << std::endl;
	try {

		//cout << "STATUS: " << cplex.getStatus() << endl;
		cout << "CUT CALLBACK called" << endl;
		IloNumArray y_values(env);
		IloNumArray f_values(env);
		cout << "BEFORE GETVALUES F" << endl;
		cout << "OBJECTIVE VALUE: " << getObjValue() << endl;

		/*LazyConsI::getValues(f_values, f);

		cout << "BEFORE GET VALUES Y" << endl;

		LazyConsI::getValues(y_values, y);
		cout << "AFTER GET VALUES" << endl;*/
		for(int u = 0; u < V0_num; u++)
			for(int v = 0; v < V0_num; v++)
			{
				// here we fail with NotExtractedExcption!!!
				if(u!=v) {
					cout << "(u,v): " << u << ";" << v << endl;
					cout << "(" << u << "," << v << "): " << IloRound(LazyConsI::getValue(y[V0_num*u + v])) << endl;
					cout << "(" << u << "," << v << "): " << IloRound(LazyConsI::getValue(y[V0_num*u + v])) << endl;
				}
			}


	} catch( IloException& e ) {

		cerr << "BaC_CutCallback: exception " << e.getMessage() << endl;
		exit( -1 );
	} catch( ... ) {
		cerr << "BaC_CutCallback: unknown exception.\n";
		exit( -1 );
	}
	//std::cout << "sepa_end" << std::endl;

	n_cuts += cuts_added;
	return cuts_added;
}
