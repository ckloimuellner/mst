CPLEX_VERSION=12.6

include /home1/share/ILOG/makefile.c++

CC=g++

ifndef O4
CCFLAGS=-pipe -Wall -Wno-deprecated $(DEFS) -g
#CCFLAGS=-pipe -Wall -Wno-deprecated $(DEFS) -pg
else
CCFLAGS=-pipe -Wall -Wno-deprecated $(DEFS) -O4
endif

CCFLAGS += $(CPLEX_CFLAGS) 
#-O4 

EXE=MST

SRCS=MST.C BaC_CutCallback.C

OBJS=$(SRCS:.C=.o)

DEPS=$(SRCS:.C=.d)

$(EXE): $(OBJS)
	$(CC) $(CCFLAGS) -o $(EXE) $(OBJS) $(CPLEX_LDFLAGS)

.SUFFIXES: .o .C .d

.C.o:
	$(CC) -c $(CCFLAGS) $<
        
.C.d:
	$(CC) -MM $(CCFLAGS) -MF $@ $<

all: $(EXE) 

clean:
	rm -f $(OBJS) $(DEPS) $(EXE)

sinclude $(DEPS)