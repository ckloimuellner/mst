#pragma once

#ifndef ILP_CUTCALLBACK_H_
#define ILP_CUTCALLBACK_H_

#include <ilcplex/ilocplex.h>

typedef IloArray<IloNumArray> IloNumArrayArray;
typedef IloArray<IloBoolVarArray> IloBoolVarArrayArray;
typedef IloArray<IloNumVarArray> IloNumVarArrayArray;
typedef IloArray<IloNumExprArray> IloNumExprArrayArray;

typedef IloArray<IloNumExprArrayArray> IloNumExprArrayArrayArray;

namespace CUT_CB {
	enum TYPE {LAZY, USER, LP};
};

class UserCutI : public IloCplex::UserCutCallbackI {

private:
	virtual void main() {
		mainUser();
	}

	virtual IloCplex::CallbackI* duplicateCallback() const {
		return duplicateCallbackUser();
	}

public:
	UserCutI(IloEnv e) : IloCplex::UserCutCallbackI(e) {}

	virtual ~UserCutI()	{}

	virtual void mainUser() = 0;
	virtual IloCplex::CallbackI* duplicateCallbackUser() const = 0;
};

class LazyConsI : public IloCplex::LazyConstraintCallbackI {

private:
	virtual void main() {
		mainLazy();
	}

	virtual IloCplex::CallbackI* duplicateCallback() const {
		return duplicateCallbackLazy();
	}

public:
	LazyConsI(IloEnv e) : IloCplex::LazyConstraintCallbackI(e) {}

	virtual ~LazyConsI() {}

	virtual void mainLazy() = 0;
	virtual IloCplex::CallbackI* duplicateCallbackLazy() const = 0;

};

class LPConsI {

private:
	virtual void main() {
		mainLP();
	}

	/*virtual IloCplex::CallbackI* duplicateCallback() const {
		return duplicateCallbackLazy();
	}*/

public:
	LPConsI() {}

	virtual ~LPConsI() {}

	virtual int mainLP() = 0;
	//virtual IloCplex::CallbackI* duplicateCallbackLP() const = 0;

};

#endif /* ILP_CUTCALLBACK_H_ */
