#ifndef BaC_CUTCALLBACK_H_
#define BaC_CUTCALLBACK_H_

#include<cstdio>
#include<iostream>
#include<vector>
#include "IlpCutCallBack.h"

using namespace std;

class BaC_CutCallback: public LazyConsI {

private:
	CUT_CB::TYPE type;

	IloCplex& cplex;
	IloEnv& env;
	double eps;
	bool lp_bound;
	IloBoolVarArray y;
	IloNumVarArray f;
	int V0_num;
	IloModel model;

	int separate();

	// separate cuts
	int findCuts();

	// current assignment, stores assigned requests for each vehicle
	std::vector<std::vector<int> > assgn;

public:
	int n_itr;	//stores the amount of iterations, i.e. how often the callback is invoked
	int n_cuts;	//stores the amount of cuts added
	int n_red;	//amount of reductions performed by solved subproblems
	double t_max;
	double t_total;

	BaC_CutCallback(IloCplex& _cplex, IloEnv& _env, IloModel& model,double _eps, int V_num, IloNumVarArray& f, IloBoolVarArray& y, bool _lp_bound);
	virtual ~BaC_CutCallback();

	// entry for lazy constraint callback (called for integer solutions)
	virtual void mainLazy() {
		type = CUT_CB::LAZY;
		env = LazyConsI::getEnv();
		separate();
	}

	virtual IloCplex::CallbackI* duplicateCallbackLazy() const {
		return (LazyConsI *) (this);
	}
};

#endif /* BaC_CUTCALLBACK_H_ */
